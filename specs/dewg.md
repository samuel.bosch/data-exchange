# Data Exchange Services Specifications

## Document Purpose

The purpose of the document is to specify specifications for Data Exchange Services, including high level architecture and key requirements for data value, trust and compliance. 

This document is dedicated to the memory of **Damien Grimonprez**, whose kindness and wisdom have been essential to this collective work.

## Definitions and Vocabulary

- `Data`: Any digital representation of acts, facts or information and any compilation of such acts, facts or information.
- `Data Catalogue`: A Data Catalogue presents a set of available Data and Data Products that can be queried.
- `Data Consumer`: A participant that receives data in the form of a Data Product. The data is used for query, analysis, reporting or any other data processing.
- `Data Exchange Services`: A set of services that provides features enabling a Data Exchange, such as and not limited to: policy negotiation for access control and usage control, exchange traceability, service protocol negotiation, data access, data tiering, access enforcement, usage enforcement.
Note: Data Connector or Data Exchange Platform are two different architecture implementations of potentially similar Data Exchange service features.
- `Data License`: A contract template which contains the constraints (terms and conditions) associated with the data included in the Data Usage Agreement. All the terms and conditions of the Data Usage Agreement must be subsumed in the Data License for all data included in the product.
- `Data Licensor`: A natural or legal participant (not necessarily a Gaia-X participant) who own usage rights for some data. It can be a data subject as per GDPR for personal data or a primary owner of non-personal data (i.e. not liable to GDPR).
- `Data Producer`: A natural or legal participant who furnishes data to a Data Product Provider.
- `Data Product`: A collection of one or more data that are packaged by the Data Product Provider and made ready for Data Exchange.
- `Data Product Usage Contract`: A legally binding agreement made by and between a Data Product Provider and a Data Consumer specifying the terms and conditions of a data exchange.
- `Data Product Description`: The Description (as per Gaia-X Architecture Document) of a Data Product. Data Product Description contains a Data License.
- `Data Product Provider`: A participant that acquires the right to access and use some data and that makes Data Products available.
- `Data Transaction`: The term "Data Transaction" symbolizes a unique unit of data transaction, logged, treated in a coherent and reliable way independent of other transactions. It is materialized by a Signed Data Product Description. The term “transaction” is preferred to “exchange” since the data asset goes from Data Product Provider to Data Consumer and there’s no exchange of data as such.
Note: there are several modes of technical data transaction, such as: Pull (direct download from an endpoint defined by the Data Product Provider), Stream (continuous download from an endpoint defined by the Data Product Provider), Push (delivery by the Data Product Provider to some endpoint defined by the Data Consumer), Publish/Subscribe (delivery of new versions of the data to some endpoint defined by the Data Consumer – usually for frequently updated data).
- `Data Usage Agreement`: A legally binding agreement made by and between a Data Producer and Data Product Provider specifying the terms and conditions to the usage of the data. Data Usage Agreement constitutes a legal usage Agreement for data which are subject to GDPR.
- `Metadata`: Data about other data, documents, or set of data that describes their content, context, structure, data format, provenance, and/or rights attached to them.
- `Term of Usage`: A specific instantiation of a Data License included in a Data Product Usage Contract listing all the constraints associated with a data exchange.

## Data Exchange Services

Data exchange in Gaia-X is enabled by a set of Data Exchange Services that are realized by each Participant and can be supported by the Ecosystem Services. 

Not all Data Exchange Services are mandatory.


1. **Authentication:** The Identities and Trust Framework are essential. Without this, you cannot connect two Participants. Identities provide general information on the Participant, and the Trust Framework appends additional claims, like verified location, or verified application of other standards or regulations.


2. **Policy negotiation and contracting** include the ability to negotiate access and usage policies between two parties. This should be a sequence between the parties, but a contracting service can support here, when one or multiple parties do not have the technical abilities for this.

   a. These policies have a focus on interoperability. All parties must be able to understand the policies to enforce them later on.
   
   b. ODRL is a used for this to support the negotiation and contracting service. 

   c. Such policies may be translated to executable policies during the transaction.


3. **A catalogue** (or metadata broker) provides mechanisms to publish metadata on a service or data as Descriptions and support search or query of the Descriptions. A catalogue may be realized as a centralized or decentralized service, but the capability can also be realized as a distributed functionality.


4. **Vocabularies** to provide additional metadata to the Descriptions. The Descriptions should contain a limited amount of information as a common denominator but must be extensible with vocabularies from different (business or technical) domains.


5. **Observability (Logging and audit data)** abilities are required to provide an auditable framework for transactions. (describe more on Logging and audit data later)


7. **Apps**, or the general ability for code 2 data.


8. **Data Exchange protocols** are required to exchange data between Participants in a distributed manner. Data exchange should be realized peer to peer and must include required metadata, e.g. for identification, authentication and authorization, but also the data contract or license.


A Participant in the data exchange should realize the interfaces to the 'services' or functionality mentioned above, but also the following functionalities internally:

- User Management.

- (Trusted) Configuration Management.

- Data and Metadata Management.

- Monitoring.

- Policy Management including the Policy Enforcement (in terms of Access and Usage Polices).

- Data App Management and Execution, i.e. the ability to execute remote code as code 2 data or the ability to make use of standard software components in a data processing pipeline.

| Service Name | Mandatory or Optional | Communication Partners |
| ---- | ---- | ---- |
| Authentication | Mandatory | Based on the Gaia-X Trust Framework OR Participant 2 Participant |
| Policy negotiation and contracting | Mandatory | Participant 2 Participant |
| Catalogue | Optional | Participant 2 Someone* providing a catalogue (to be explained) |
| Vocabularies | Optional | Participant 2 Someone* providing a Vocabulary hub (to be explained) |
| Observability | Optional | Participant 2 Someone* providing an "Observer Facility" (to be explained) |
| Apps | Optional | To be understand better before we define this |
| Data Exchange protocols | One is required, but no mandatory protocol *2 | Participant 2 Participant |
| User Management (internal) | Optional | Internal only |
| Data and Metadata Management  (internal) | Optional | Internal only |
| Monitoring (internal) | Optional | Internal only |
| Policy Management including the Policy Enforcement (internal) | Optional | Internal only |
| Data App Management (internal) | Optional | Internal only |

&ast; Someone must be explained: It has to be something and someone the Ecosystem trusts in. A Trust Anchor.<br />
&ast;2 Specified by the negotiated contract.


## Data Product Conceptual Model and Data Usage Operational Models

The `Data Product` conceptual model and the `Data Usage` operational model are described in the `Data Exchange Services` chapter of the `Gaia-X Architecture Document`.

## Ontologies for Data Exchange

### Participants

The `Data Exchange` conceptual model specify particular roles for the participant of a Gaia-X Ecosystem. When possible, the corresponding role/type in the Gaia-X Conceptual Model and Trust Framework is précised.
The role has an importance when defining the following Data Objects and rules.

| Object | SubclassOf | Comment |
| ------ | ---------- | ------- |
| DataProductProvider | ['gx:LegalPerson'] | is equivalent to Gaia-X Provider.|
| DataLicensor | ['gx:LegalPerson'] | is equivalent to Gaia-X Licensor. |
| DataProducer | ['gx:LegalPerson'] | is equivalent to Gaia-X RessourceOwner. |
| DataConsumer | ['gx:LegalPerson'] | is equivalent to Gaia-X Consumer. |

The attributes for the different participant are of type ['gx:LegalPerson'] as defined in the TrustFramework https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/#legal-person

<!-- do we delete this, if the attributes are defined elsewhere? -->

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| [`parentOrganisation`](https://schema.org/parentOrganization) | participant[] | No | A list of direct `participant` that this entity is a subOrganization of, if any.  | 
| `name` | String | Yes | Name of `participant`. | 
| `registrationNumber` | String | Yes | Country’s registration number which identifies one specific company. |
| `LEI Code` | String | No | Unique LEI number as defined by https://www.gleif.org. |
| `headquarterAddress` | String  | Yes | Physical location of head quarter in [ISO 3166-2](https://www.iso.org/standard/72483.html) alpha2, alpha-3 or numeric format. |
| `headquarterAddress.street-address` | String  | No | Street Address. |
| `headquarterAddress.postal-code` | String  | No | Postal Code. |
| `headquarterAddress.region` | String | No | Region. | 
| `headquarterAddress.locality` | String | No | Locality. | 
| `headquarterAddress.country-name` | String | Yes | Country Name. |
| `legalAddress` | String | Yes | Physical location of legal quarter in ISO 3166-1 alpha2, alpha-3 or numeric format. |
| `legalAddress.street-address` | String  | No | Street Address. |
| `legalAddress.postal-code` | String  | No | Postal Code.  |
| `legalAddress.region` | String | No | Region. | 
| `legalAddress.locality` | String | No | Locality. | 
| `legalAddress.country` | String | Yes | Country. |

### Data Product

A `Data Product` consists of the characterization of the actual data as well a description of the contractual part. At minimum, a `Data Product Description` needs to contain all information so that a `Data Consumer` can initiate a contract negotiation. All other attributes that are used to describe the data are optional. However, the `Data Product Provider` has an interest to precisely describe the data so that it can be found and consumed. If a `Data Product` is published in a catalogue, the `Data Product Provider` might precisely describe the `Data Product Description` so that it can be found and consumed by `Data Consumers`.

A `Data Product Description` includes a list of one or many `Dataset` which includes a list of one or many `Distribution`.

#### Data Product Description

A `Data Product Description` is inheriting the attributes of a `Service Offering` as defined by the Trust Framework  [Service Offering](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/service_and_subclasses/#service-offering), and also extending the [DCAT-3 Cataloged Resource class](https://www.w3.org/TR/vocab-dcat-3/#Class:Resource).


| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------| ------ |
| `gx:providedBy` | URI | Yes | A resolvable link to the participant Description providing the service. | 
| `gx:termsAndConditions` | URI | Yes | A resolvable link to the Terms and Conditions applying to that service. Terms and conditions may include Business Conditions and SLA's that apply to the `Data Product`. |
| `dct:license`  | String[] | Yes | A list of URIs to license documents. |
| `dct:title` or `gx:title` | String |Yes | Title of the Data Product. |
| `dct:description` or `gx:description` | String | No | Description of the Data Product. |
| `dct:issued` | Date - ISO 8601 | No | Publication date in ISO 8601 format.|
| `gx:obsoleteDateTime`   | Date - ISO 8601 | No | Date time in ISO 8601 format after which the Data Product is obsolete. |
| `odrl:hasPolicy`      | policy in ODRL | No | `Policy` expressed using ODRL. |
| `gx:dataLicensors`     | URI[] | No | A list of Licensors either as a free form string or `participant` Description.|
| `gx:dataUsageAgreement` | DataUsageAgreement[]  | No  | List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework. |
| `gx:aggregationOf`  | dcat:dataset[] or gx:dataset[] | Yes | DataSet Content. |
| `dct:identifier`  | String | Yes | Unique uuid4. |
| `dcat:contactPoint` | Format vCard [VCARD-RDF] | No | Contacts to get more information. |
| `dcterms:conformsTo` | URI[] |  No | A list of established standards to which the described resource conforms. |

**Note: Terms and Data Usage Agreement are two distinct concepts.** 

The `gx:termsAndConditions` are mandatory to establish the way the service is consumed and contracted. In addition, the `gx:dataUsageAgreement` is optional and describes the list of authorizations from the `Data Licensor`.
In case of PII, the Data Controller Participants (as defined in GDPR) are listed under `gx:dataLicensors`.

**Note:** If Terms and Conditions include Business Conditions and/or SLA's, it is the responsibility of the Data Product Provider to provide ODRL descriptions for these terms, for them to be automatically checked. 

**Note: Domain Specific Attributes**
Additional attributes that are important to describe a Data Product but should be defined in relation with an application domain's needs to be defined by Ecosystems by extending the model per Data Domain and not at the Gaia-X level. As an example, `dcat:theme` or `dcat:keyword` requires a specific domain vocabulary. It has been defined at the Ecosystem Level. 

It is recommended to extend a `Data Product Description` at least by the following attributes:

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------| ------ |
| `dcat:keywords` | String[] | No | List of Tags or Keywords (Unicode) for data domains. |
| `dcat:theme` | String[] | No | A main category of the resource. A resource can have multiple themes. |
| `dcterms:type` | String[] | No | The nature or genre of the resource. |

**Consistency rules**

- The keypair used to sign the Data Resource claims must be traceable to the `gx:producedBy` participant of the Data Resource.
- If the data are about data subjects as one or more Natural Persons, or sensitive data as defined in GDPR [article 9](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2051-1-1), than `dataController` and `DataUsageAgreement` are mandatory. 
<!-- `dataController` and `Agreement` are not listed in the table. Should they be added? --> 
  To avoid [data re-identification](https://ec.europa.eu/eurostat/cros/content/re-identification_en), this rule applies independently if the data is raw, pseudo-anonymized or anonymized. (Note: This is on purpose beyond GDPR requirements.)
- If `gx:dataLicensors` is specified, the `Data Licensor` should be the issuer of the Data Resource `gx:dataUsageAgreement`. Claims must be traceable and verifiable against Gaia-X Compliance. 

#### Dataset Description

A `Dataset` is extending the attributes of [DCAT-3 Dataset class](https://www.w3.org/TR/vocab-dcat-3/#Class:Dataset).
 
| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------| ------ |
| `dct:title`| String |Yes | Title of the Data Product. |
| `dct:distributions` | dcat:distribution[] | Yes | List of distributions format of the dataset. |
| `dct:identifier`  | String | Yes | Unique uuid4. |
| `dct:issued` | Date - ISO 8601 | No | Publication date in ISO 8601 format.|
| `gx:expirationDateTime` | Date - ISO 8601 | No  | Date time in ISO 8601 format after which data is expired and shall be deleted. |
| `dct:license`  | String[] | No | A list of URIs to license document. |
| `gx:dataLicensors`     | URI[] | No | A list of Data Licensors either as a free form string or `participant` Description.|
| `gx:dataUsageAgreement` | DataUsageAgreement[]  | No  | List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework. |
| `gx:exposedThrough`  | URI[] | Yes | A resolvable link to the data exchange component that exposes the Data Product. |

#### Distribution Description

A `Dataset` is extending the attributes of [DCAT-3 Distribution class](https://www.w3.org/TR/vocab-dcat-3/#Class:Distribution).

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | -------|
| `dct:title`| String | Yes | Title of the Data Product. |
| `dct:format` | Standard IANA | Yes | Format of the dataset distribution (pdf, csv, …). |
| `dcat:compressFormat` | Standard IANA | No | The compression format of the distribution in which the data is contained in a compressed form. |
| `dcat:packageFormat` | Standard IANA | No | The package format of the distribution in which one or more data files are grouped together. |
| `dcat:byteSize` | String | No | Size of the dataset distribution. |
| `gx:location` | String[] | No | List of dataset storage location. |
| `gx:hash` | String | No | To uniquely identify the data contained in the dataset distribution.|
| `gx:hashAlgorithm` | String | No | Hash Algorithm.|
| `dct:issued` | Date - ISO 8601 | No | Publication date in ISO 8601 format.|
| `gx:expirationDateTime` | Date - ISO 8601 | No  | Date time in ISO 8601 format after which data is expired and shall be deleted. |
| `dcat:language`	 | Lang : ISO 639-1:2002 | No | Language.  |
| `dct:license`  | String[] | No | A list of URIs to license document. |
| `gx:dataLicensors`     | URI[] | No | A list of Licensors either as a free form string or `participant` Description.|
| `gx:dataUsageAgreement` | DataUsageAgreement[]  | No  | List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework. |

#### Data Catalogue

In Gaia-X Trust Framework, a `catalogue` is a subclass of a `Service Offering` with one additional attribute providing the route to retrieve the list of Verifiable Credentials IDs [Catalogue](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/service_and_subclasses/#catalogue).

As such, a `Data Catalogue` directly inherits the `Catalogue` class.

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | -------|
| `gx-trust-framework:getVerifiableCredentialsIDs`| String | Yes | A route used to synchronize catalogues and retrieve the list of Verifiable Credentials (issuer, id). |

In DCAT, a catalogue is a list of catalog records [service, dataset, resources, catalogue] descriptions [DCAT-3 Catalog class](https://www.w3.org/TR/vocab-dcat-3/#Class:Catalog).

If all the records of a DCAT Catalog are exposed as Verifiable Credentials through IDs in a Gaia-X Catalogue, then both object are conceptually similar.

### Data Transaction

Data Transaction is comprised of a `Data Product Usage Contract` and a `Data Usage`.

A `Data Product Usage Contract` is based on the `Data Product Description` but may possibly differ from the original one after modification during the negotiations. It is signed by both `Data Product Provider` and `Data Consumer`. It must include `Terms of Usage`. For each `Dataset` included in the `Data Product`, the `Data Product Usage Contract` must include an explicit `Data Usage Agreement`.

The `Data Product Usage Contract` is a Ricardian contract. The parties can (optionally) request this contract to be notarized in a `Data Product Usage Contract Store`. 

After such contract has been agreed upon and has been signed by both parties, the `Data Consumer` can start accessing and using the data, realizing the `Data Product Usage Contract`. Such `Data Usage` with the associated `Data Product Usage Contract` corresponds to a `Data Transaction`.

The contract negotiation can lead to both parties agreeing on a `Data Usage Logging Service`.

`Signature Check Type`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:participantRole` | String | Yes | Establish a unique way to identify the participant that has to Sign (e.g. `gx:providedBy` is identified by `Provider` ). Possible values are Provider, Consumer, Licensor, Producer. | 
| `gx:mandatory` | String | Yes | Establish  if a Signature is mandatory or Optional. Possible values are Yes/No. | 
| `gx:legalValidity` | String| Yes | Establish if the legal validity check needs to be enforced to the Signature. Possible values are Yes/No. |

`Data Product Usage Contract`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:providedBy` | URI | Yes | A resolvable link to the `Data Product Provider`. | 
| `gx:consumedBy` | URI | Yes | A resolvable link to the `Data Consumer`. | 
| `gx:dataProduct` | URI | Yes | A resolvable link to the `Data Product Description` (after negotiation). |
| `gx:signers` | SignatureCheckType[]| Yes | The array identifying all required Participant signatures.|
| `gx:termOfUsage` | URI | Yes | A resolvable link to the Term of Usage. |
| `gx:notarizedIn` | URI | No | A resolvable link to the Notarization service. |
| `odrl:hasPolicy`  | policy in ODRL | No | `Policy` Agreement expressed using ODRL. |

`Data Usage`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:loggingService` | URI | No | Link to the Logging Service. |
| `gx:dataUsageContract` | URI | Yes | A resolvable link to Data Usage Contract. | 


**Note:** Logging Service shall monitor significant parameters and history logs in order to enforce Terms and Conditions (Business Conditions and SLA's) corresponding (if defined) to the ORDL policy (territories, industries, usages, expiration, ...). 

`Data Usage Agreement`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:producedBy` | URI | Yes | A resolvable link to the `Data Producer`. | 
| `gx:providedBy` | URI | Yes | A resolvable link to the `Data Product Provider`. | 
| `gx:licensedBy` | URI[] | No | A list of resolvable links to `Data Licensors`. | 
| `gx:dataUsageAgreementTrustAnchor` | URI | Yes | A resolvable link to the `Data Usage Agreement Trust Anchor`. | 
| `gx:dataProduct` | URI | Yes | A resolvable link to the `Data Product Description`. |
| `gx:signers` | SignatureCheckType[]| Yes | The array identifying all required Participant signatures.|

## Policies for Data Exchange

Policies for data exchange shall reflect different aspects to specify terms and conditions for the data and the exchange of the data. Therefore, such policies have a different scope and concern.

1. Contract Policies that are interoperable to be clearly and unambiguous as a basis for a contract between the participants. This contract policies should be machine and human readable. It must be able to contain access and usage policies. [ODRL](https://www.w3.org/TR/odrl-model/) is used as a Policy Definition Language for this purpose. 
2. Runtime Policies are derived from the Contract Policies and are used for the execution of the contract policies in the system of the participants. Options for Policy Definition Languages for execution are [Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) or [XACML](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=xacml).

For the Data Exchange the focus is on (1) the Contract Policies. The contract is negotiated between the participants of the data exchange by making use of a contract negotiation sequence [like specified by IDSA](https://github.com/International-Data-Spaces-Association/IDS-RAM_4_0/blob/main/documentation/3_Layers_of_the_Reference_Architecture_Model/3_3_Process_Layer/3_3_3_Contract_Negotiation.md) or [GXFS-DE](https://www.gxfs.eu/download/1737/). The result is a signed contract between the two parties, which is a Description of the data asset and the contract as verifiable credential. 

The contract policies contain at least:
1. General description of the data asset, the involved parties and general terms.
2. Access policies describing the requirements and rules for access to the data at the Data Product Provider sides.
3. Usage policies as obligations for the data consumer sides.
4. Signatures.

**Usage control** is an extension to traditional access control. It is about the specification and enforcement of restrictions regulating what must (not) happen to data.

Thus, usage control is concerned with requirements that pertain to data processing (obligations), rather than data access (provisions). Usage control is relevant in the context of intellectual property protection, compliance with regulations, and, more generally, digital rights management.

**Access control** restricts access to resources. The term authorization is the process of granting permission to resources. 

Resource owners define attribute-based access control policies for their endpoints and define the attribute values a subject must attest in order to grant access to the resource.

In contrast to access control, the overall goal of usage control is to enforce usage restrictions for data after access has been granted. Therefore, the purpose of usage control is to bind policies to data being exchanged.
The following specifications ([extracted from IDSA Position Paper about Usage Control](https://internationaldataspaces.org/download/21053/)) are examples of policy classes:

- Allow the Usage of the Data (provides data usage without any restrictions).
- Interval-restricted Data Usage (provides data usage within a specified time interval).
- Duration-restricted Data Usage (allows data usage for a specified time period).
- Location Restricted Policy.
- Perpetual Data Sale (Payment once).
- Data Rental (Payment frequently).
- Role-restricted Data Usage.
- Purpose-restricted Data Usage Policy.
- Restricted Number of Usages (allow data usage for n times).
- Security Level Restricted Policy (allow data access with a specified security level).
- Use Data and Delete it After (allows data usage within a specified time interval with the restriction to delete it at a specified time stamp).
- Attach Policy when Distribute to a Third-party.
- Distribute only if Encrypted.

To express and execute the Contract Policies different information are required during runtime to evaluate the policies. To do so, at least three different information models are required:

- The generic Ecosystem/Gaia-X policy data models for basic discovery and trust negotiation policies.
- The per Ecosystem/Industry specific data model which needs to be understood by all participants of the Ecosystem.
- The per data contract/data asset specific data model which might be irrelevant for someone who does not receive the data but crucial for someone who has to understand the usage restrictions of a specific contract. 


### ODRL

**1 - Concepts**

Links:
- [ODRL Information Model 2.2](https://www.w3.org/TR/odrl-model/)
- [ODRL Vocabulary & Expression 2.2](https://www.w3.org/TR/odrl-vocab/)
- [ODRL Implementation Best Practices](https://w3c.github.io/odrl/bp/)
- [ODRL Profile Best Practices](https://w3c.github.io/odrl/profile-bp/)

Open Digital Rights Language (ODRL) is a model for describing the usage of content, including authorized, prohibited, and mandatory actions, the resources on which the actions apply, the actors and participants involved in the actions, usage conditions, and additional information such as responsibilities and regulations. 

It allows the implementation of usage control features that complement the access controls usually in place. These usage controls come from licenses and are applied before using data, so they are dependent on each data and its usage environment. Some examples of usage control include authorization of usage for a specific period, anonymization of data before processing, and prohibition of data transfer under certain conditions.

The main concepts are the following:

**Policy**: A policy is a group of rules (which can be permissions, prohibitions, or obligations). It has three child classes:
 
- **Set**: A generic collection of rules.
- **Offer**: A collection of rules that are offered by an actor designated by the `assigner` property.
- **Agreement**: A collection of rules that have been agreed upon by an actor designated as `assignee` and another actor designated as `assigner`.

![Capture_d_écran_2023-04-21_à_08.48.20](./media/odrl-diagram/Capture_d_écran_2023-04-21_à_08.48.20.png)

**Rule**: Rule is the base class defining the rules of policies. It has the following properties: 
- asset, which defines the target resource, 
- action, which defines the operation to be performed on the resource, 
- constraint, which defines the conditions for the rule's validity (e.g., date > 2020), and
- party, which defines the actors involved in the rule.

It has three child classes:

- **Permission**: This rule grants permission for the actor to perform the action.
- **Duty**: This rule obliges the actor to perform the action.
- **Prohibition**: This rule prohibits the actor from performing the action

![Capture_d_écran_2023-04-21_à_08.51.10](./media/odrl-diagram/Capture_d_écran_2023-04-21_à_08.51.10.png)

The Duty class can also be used to specify a rule:

- In a permission, it specifies pre-conditions that must be met before granting permission.
- In an obligation, it specifies an action to be performed if the obligation is not fulfilled.
- In a prohibition, it specifies an action to be performed if the prohibition is not respected.

![Capture_d_écran_2023-04-21_à_08.53.41](./media/odrl-diagram/Capture_d_écran_2023-04-21_à_08.53.41.png)

- **Asset**: An asset is a resource or a collection of resources. An asset is the target of a rule to which it applies. A collection can have constraints that allow filtering of the elements of the collection to which a rule applies.

![Capture_d_écran_2023-04-21_à_08.55.35](./media/odrl-diagram/Capture_d_écran_2023-04-21_à_08.55.35.png)

- **Action**: An action is an operation that can be performed on an asset. ODRL defines, among others, the two main actions: "use" and "transfer". Other actions can be defined in a specific vocabulary.
- **Constraint**: Constraints are logical expressions that allow filtering of different collections. ODRL defines the following logical operators: `or`, `xone`, `and`, and `andSequence`. Comparison operators are defined in the ODRL vocabulary: [ODRL Vocabulary & Expression 2.2#Constraint Operators](ODRL Vocabulary & Expression 2.2#Constraint Operators.)

![Capture_d_écran_2023-04-21_à_08.58.39](./media/odrl-diagram/Capture_d_écran_2023-04-21_à_08.58.39.png)

- **Party**: A party is an actor or collection of actors who have a functional role in a rule. The actor can have the role of `assignee` (recipient of the rule) or `assigner` (issuer of the rule). A collection can have constraints that allow filtering of the elements of the collection to which a rule refers.

![Capture_d_écran_2023-04-21_à_09.00.02](./media/odrl-diagram/Capture_d_écran_2023-04-21_à_09.00.02.png)

Additional metadata can be used to specify policies. Dublin Core Metadata is recommended for this purpose, for example:
- dc:creator: author of the rule.
- dc:description: description of the rule.
- dc:issued: publication date.
- dc:modified: date of last update.
- dc:replaces: identifier of a policy that is replaced.
- dc:isReplacedBy: identifier of a policy that replaces the current policy.

The policies can use inheritance to reuse existing rules. In case of conflict between the rules, the `conflict` property allows specifying the desired behavior. The vocabulary of ODRL can be extended by using profiles, for example: additional actions, roles of actors.

**2 - Examples**

**File exchange**:

The ODRL file uses the following terms:
- Utilization : https://www.w3.org/TR/odrl-vocab/#term-use
- File transfer : https://www.w3.org/TR/odrl-vocab/#term-distribute
- Payment : https://www.w3.org/TR/odrl-vocab/#term-compensate

```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "duty": [
       {
         "assigner": "Data Provider",
         "assignee": "Data Consumer",
         "action": [
           {
             "value": "compensate",
             "refinement": [
               {
                 "leftOperand": "payAmount",
                 "operator": "eq",
                 "rightOperand": { "@value": "500.00", "@type": "xsd:decimal" },
                 "unit": "https://dbpedia.org/resource/Euro"
               }
             ]
           }
         ]
       }
     ]
   }
 ],
 "obligation": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assigner": "Data Consumer",
     "assignee": "Data Provider",
     "action": "distribute"
   }
 ]
}
```

**License Exclusivity**: The term `ensureExclusivity` allows to specify an exclusive license:
the aim of `ensureExclusivity` is to ensure that a rule applied to an asset maintains exclusivity. When used as a 'Duty' it explicitly designates the assignee responsible for upholding this exclusivity.
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "obligation": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Provider",
     "action": "ensureExclusivity"
   }
 ]
}
```

**Territories**: the term `odrl:spatial` is used to specify territories. 
Restrict the use to the specified territories using the operator `isAnyOf`: in the bellow example `isAnyOf` indicating that a given value is any of the right operand of the Constraint.
Here, the right operand is defined as a list of values, specifically `fr` ( for French) and `es` ( for Spanish). The condition will evaluate to true if the `odrl:spatial` aspect matches any of these values.  

```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "odrl:spatial",
         "operator": "isAnyOf",
         "rightOperand": {
           "@list": [ "fr", "es" ]
         }
       }
     ]
   }
 ]
}
```
Restrict the use outside of the specified territories with the operator `isNoneOf`: Its purpose is to determine whether a given value is not part of the set defined by the right operand of a constraint. In the bellow example `isNoneOf` indicating that the assigned data consumer is restricted from using the data in locations categorized as neither French (`fr`) nor Spanish (`es`).
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "odrl:spatial",
         "operator": "isNoneOf",
         "rightOperand": {
           "@list": [ "fr", "es" ]
         }
       }
     ]
   }
 ]
}
```
**Industries**: The term `odrl:industry` is used to specify business sectors. This allows ODRL to apply actions within the specified industry context, such as publishing or the financial industry.
Restrict the use to specified business sectors using the `isAnyOf` operator:
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "odrl:industry",
         "operator": "isAnyOf",
         "rightOperand": {
           "@list": [ "automotive" ]
         }
       }
     ]
   }
 ]
}
```

Restrict usage outside of the specified industries using the operator `isNoneOf`:

```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "odrl:industry",
         "operator": "isNoneOf",
         "rightOperand": {
           "@list": [ "automotive" ]
         }
       }
     ]
   }
 ]
}
```

**Usages**: the term `odrl:product` is used to specify uses to categorize or specify the type of product or service, providing context for the enforcement of a rule.
Restrict usage to the specified uses with the `isAnyOf` operator:

```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "odrl:product",
         "operator": "isAnyOf",
         "rightOperand": {
           "@list": [ "statistics" ]
         }
       }
     ]
   }
 ]
}
```

Restrict the use outside of the specified uses with the operator `isNoneOf`:
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "odrl:product",
         "operator": "isNoneOf",
         "rightOperand": {
           "@list": [ "statistics" ]
         }
       }
     ]
   }
 ]
}
```
**Expiration**: The term `dateTime` The date (and optional time and timezone) of exercising the action of the Rule. Right operand value MUST be an `xsd:date` or `xsd:dateTime` as defined by xmlschema. This vocabulary here is used to specify an expiration date for the permission:
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "constraint": [
       {
         "leftOperand": "dateTime",
         "operator": "lt",
         "rightOperand": {
           "@value": "2023-01-01",
           "@type": "xsd:date"
         }
       }
     ]
   }
 ]
}
```

**Sub licensing**: The term `grantUse` allows managing the possibilities of sub-licensing. The main aim of `grantUse` is to enable the assignee to grant the use of the asset to third parties. It allows the assignee to create policies that govern how the asset can be used by third parties. This action is often used when the owner of the asset wants to authorize others to use it under certain conditions or restrictions, and it may involve specifying additional policies or constraints for third-party use.







No sub-licensing right:
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "prohibition": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "grantUse"
   }
 ]
}
```
Right to sublicense without restriction:
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "grantUse"
   }
 ]
}
```
Sub-licensing to subsidiaries: the `refinement` property is used to restrict the action:
```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": [
       {
         "value": "grantUse",
         "refinement": [
           {
             "leftOperand": "recipient",
             "operator": "eq",
             "rightOperand": "subCompanies"
           }
         ]
       }
     ]
   }
 ]
}
```

Example with:
- Territorial restriction.
- Restriction on industry sectors.
- Usage restriction.
- Time limit.
- Right to sub-license to subsidiaries without sub-licensing.

```
{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": "use",
     "duty": [
       {
         "assigner": "Data Provider",
         "assignee": "Data Consumer",
         "action": [
           {
             "value": "compensate",
             "refinement": [
               {
                 "leftOperand": "payAmount",
                 "operator": "eq",
                 "rightOperand": { "@value": "500.00", "@type": "xsd:decimal" },
                 "unit": "https://dbpedia.org/resource/Euro"
               }
             ]
           }
         ]
       },
       {
         "action": "nextPolicy",
         "target": "https://data-exchange.com/policy:1010"
       }
     ],
     "constraint": [
       {
         "leftOperand": "odrl:spatial",
         "operator": "isAnyOf",
         "rightOperand": {
           "@list": [ "fr", "es" ]
         }
       },
       {
         "leftOperand": "odrl:industry",
         "operator": "isAnyOf",
         "rightOperand": {
           "@list": [ "automotive" ]
         }
       },
       {
         "leftOperand": "odrl:product",
         "operator": "isAnyOf",
         "rightOperand": {
           "@list": [ "statistics" ]
         }
       },
       {
         "leftOperand": "dateTime",
         "operator": "lt",
         "rightOperand": {
           "@value": "2023-01-01",
           "@type": "xsd:date"
         }
       }
     ]
   }
 ],
 "obligation": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assigner": "Data Consumer",
     "assignee": "Data Provider",
     "action": "distribute"
   }
 ]
}


{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:1010",
 "permission": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "assignee": "Data Consumer",
     "action": [
       {
         "value": "grantUse",
         "refinement": [
           {
             "leftOperand": "recipient",
             "operator": "eq",
             "rightOperand": "subCompanies"
           }
         ]
       }
     ],
     "duty": [
       {
         "action": "nextPolicy",
         "target": "https://data-exchange.com/policy:nosublicence-123"
       }
     ]
   }
 ]
}


{
 "@context": "https://www.w3.org/ns/odrl.jsonld",
 "@type": "Set",
 "uid": "https://data-exchange.com/policy:nosublicence-123",
 "prohibition": [
   {
     "target": "https://data-exchange.com/dataset/123",
     "action": "grantUse"
   }
 ]
}
```
