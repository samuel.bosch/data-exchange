# Data Exchange Services

## Presentation of working group

The objective of the Data Exchange Services produce specifications for Data Exchange Services between different domains (organizations, security domains, ...). It includes high level architecture and key requirements for data value, trust and compliance. 

This collaborative work is conducted in the context of Gaia-X and the Gaia-x Architecture. 

The mission of this working group is described in the [Data Exchange Services Mission Document](https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/blob/master/docs/mission_documents/technical-committee_federation-services_data-exchange.yaml)

## Meetings schedule

* Every Monday at 10am on [Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_YWQxOWZiZGQtMzc1NS00MjEzLTk0MTctMzIxMjY3NTk5OGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22f1334478-d7b8-4b5e-b589-c5351a2d8cb0%22%7d)

## Specifications

The (work in progress) specification document is available [here](/specs/dewg.md).

The redactional work is provided by participants on dedicated branches and then submitted for merge request.

## Current working group objectives

* First draft of specifications by June 2022 (scope of work)
* First release of specifications by end of July 2022

## Output

The output is published here: https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange 
